package org.elu.learn.groovy

import groovy.transform.CompileStatic
import groovy.transform.TypeChecked

println 'Groovy 101'

// compact syntax
// Java
for (String it : new String[]{"Arno", "Lauri", "Alisa", "Karoliina"}) {
    if (it.length() <= 4) {
        System.out.println(it);
    }
}

// Groovy
['Arno', 'Lauri', 'Alisa', 'Karoliina'].findAll { it.length() <= 4 }.each { println it }
println()

// dynamic def
// List and Map definition
def list = [1, 2]
def map = [cars: 1, boats: 2]
println list.getClass()
println map.getClass()

map.cars = 2
map.boats = 3
map.planes = 0
println map.cars

class Vampire {
    String name
    int year
}

list = []
list.add(new Vampire(name: "Count Dracula", year: 1987))
list << new Vampire(name: "Count Dracula", year: 1987)
list += new Vampire(name: "Count Dracula", year: 1987)
println list.size()
println()

// Groovy GDK
["Java", "Groovy", "Scala"].each { println it }

// Everything Is an Object
5.times { println 'hi' }
println()

// Easy Properties
class Person {
    String firstName
    String lastName
}
def person = new Person(firstName: "Ed", lastName: "Swiss")
println person.firstName
person.firstName = 'Janna'
println person.firstName
println person.properties
println()

// GString
def os = 'MacOS'
def cores = 4
println("Cores: $cores, OS: $os, Time: ${new Date()}")
println()

// Closures
def print1 = { lst ->
    lst.each{ println owner } //prints first Closure
    println owner //prints the enclosing class
}
print1([1,2])

list = [ 'foo', 'bar' ]
def newList = []
list.collect(newList) {it.toUpperCase() }
println newList

def  closr = {x -> x + 1}
println( closr(2) ) // prints 3
println closr.getClass()
println()

// Better switch
def x = 42
switch (x) {
    case 'foo':
        result = 'found foo'
        break
    case [4, 5, 6]:
        result = '4, 5 or 6'
        break
    case 12..30: // Range
        result = '12 to 30'
        break
    case Integer:
        result = 'was integer'
        break
    case Number:
        result = 'was number'
        break
    default:
        result = 'default'
}

// Switch in Groovy 4
def result = switch (x) {
    case 'foo' -> 'found foo'
    case [4, 5, 6] -> '4, 5 or 6'
    case 12..30 -> '12 to 30'
    case Integer -> 'was integer'
    case Number-> 'was number'
    default -> 'default'
}
println result
println()

// Meta-Programming
// for entire class
String.metaClass.upper = { -> toUpperCase() }
// or for single instance
def str = 'test'
str.metaClass.upper = { -> toUpperCase() }
println str.upper()
println str.toUpperCase()
println()

// Static Type Checking
@TypeChecked
class Foo {
    //int x = 42.0  // this does not compile
}
@TypeChecked
class Bar {
    int x = 42  // this works fine
}

@CompileStatic
@TypeChecked
class Foo2 {
    static void getFibs(int count) {
        def list = [0, 1] // print first #count Fibonacci numbers
        count.times {
            print "${list.last()} "
            list << (Integer) list.sum()
            list = list.tail()
        }
    }
}
Foo2.getFibs(5)
println()

// Elvis operator
// Java
String name = person.getFirstName() == null ? "Ed"  : person.getFirstName()
println name
// Groovy
name = person.lastName ?: 'Finn'
println name
println()

// Safe Dereference Operator
Person anotherPerson
println anotherPerson?.firstName
anotherPerson = new Person(firstName: 'Arno')
println anotherPerson?.firstName
println anotherPerson?.lastName ?: 'Finn'
